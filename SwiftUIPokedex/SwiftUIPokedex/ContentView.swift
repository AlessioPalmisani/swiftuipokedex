//
//  ContentView.swift
//  SwiftUIPokedex
//
//  Created by Alessio Palmisani on 19/01/2021.
//

import SwiftUI

struct ContentView: View {
    
//    init() {
//        UINavigationBar.appearance().backgroundColor = .systemRed
//        UINavigationBar.appearance().largeTitleTextAttributes = [
//            .foregroundColor: UIColor.white]
//        UINavigationBar.appearance().titleTextAttributes = [
//            .foregroundColor: UIColor.white]
//    }
    
    @ObservedObject var networkManager = NetworkManager()
    @ObservedObject var homeDataSource = HomeDataSource()
    @State var detail : [Details] = []
    
    var body: some View {
//        NavigationView {
//
//            List(homeDataSource.names.indices, id: \.self) { index in
//                NavigationLink(destination: DetailView(name: homeDataSource.names[index])) {
//                    HStack {
//                        UrlImageView(urlString: detail[index].sprites?.frontDefault)
//                        Text("#\(homeDataSource.pkmnId[index])")
//                        Text(homeDataSource.names[index].capitalized)
//                    }.onAppear(){
//                    self.networkManager.getDetails(url: "https://pokeapi.co/api/v2/pokemon/\(homeDataSource.names[index])") { (details) in
//                        self.detail.append(details)
//                    }                }
//            }.navigationBarTitle("Pokedex")
//        }.accentColor(.white)
//    }

        PokeListView(homeDataSource: homeDataSource)

    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

