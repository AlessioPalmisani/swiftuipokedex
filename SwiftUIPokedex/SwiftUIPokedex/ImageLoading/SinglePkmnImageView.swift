//
//  SinglePkmnImageView.swift
//  SwiftUIPokedex
//
//  Created by Alessio Palmisani on 21/01/2021.
//

import SwiftUI



struct SinglePkmnImageView: View {
    
    
    @ObservedObject var imageLoader = ImageLoader(urlString: "")
    @ObservedObject var networkManager = NetworkManager()
    @State var details: Details?
    var name = ""
    

    
    var body: some View {
        
 
        
        
        ZStack {
            Image("PokeWhite")
                .resizable()
                .scaledToFit()
                .frame(width: 30, height: 30)
                .rotationEffect(.degrees(-30))
            Image(uiImage: imageLoader.image ?? UrlImageView.defaultImage!)
                .resizable()
                .scaledToFit()
                .frame(width: 50, height: 50)
        }
        }
    }


struct SinglePkmnImageView_Previews: PreviewProvider {
    static var previews: some View {
        SinglePkmnImageView()
    }
}
