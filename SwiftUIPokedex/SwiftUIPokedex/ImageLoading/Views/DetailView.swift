//
//  DetailView.swift
//  SwiftUIPokedex
//
//  Created by Alessio Palmisani on 20/01/2021.
//

import SwiftUI



struct DetailView: View {
    
    
    @ObservedObject var networkManager = NetworkManager()
    @State var selected = 0
    var name = ""
    @State var details: Details?
    @State var typesArray: [String] = []
    @State var species: Species?
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var body: some View {
        ZStack(alignment: .topLeading){
            VStack{
                
                HStack(alignment: .center){
                    UrlImageView(urlString: details?.sprites?.frontDefault ?? details?.sprites?.other?.officialArtwork?.front_default)
                    Spacer()
                    
                    VStack(alignment: .leading) {
                        Text("#\(details?.id ?? 0)")
                            .padding(.bottom)
                            .foregroundColor(.black)
                        
                        Text(name.capitalized)
                            .padding(.bottom)
                            .foregroundColor(.black)
                        
                        if typesArray.count == 1 {
                            Text("   \(typesArray.first ?? "Can't find type")   ")
                                .background(RoundedRectangle(cornerRadius: 4).foregroundColor(Color("\(typesArray.first ?? "normal")")))
                        } else if typesArray.count == 2 {
                            HStack {
                                Text("   \(typesArray.last ?? "Can't find type")   ")
                                    .background(RoundedRectangle(cornerRadius: 4).foregroundColor(Color("\(typesArray.last ?? "normal")")))
                                    .foregroundColor(.white)
                                Text("   \(typesArray.first ?? "Can't find type")   ")
                                    .background(RoundedRectangle(cornerRadius: 4).foregroundColor(Color("\(typesArray.first ?? "normal")")))
                                    .foregroundColor(.white)
                            }
                        }
                        
                    }
                }
                
                ZStack(alignment: .center) {
                    RoundedRectangle(cornerRadius: 25)
                        .frame(width: screenWidth, height: (screenHeight * (3/4)))
                        .foregroundColor(.white)
                        .edgesIgnoringSafeArea(.bottom)
                    
                    VStack(alignment: .leading, spacing: 20) {
                        // STATS GRAPHS
                        Group {
                            Text("Base Stats")
                                .foregroundColor(Color("\(typesArray.last ?? "normal")"))
                                .padding(.top, 10)
                            
                            BarView(color: "\(typesArray.last ?? "normal")", stat: "HP", statValue: CGFloat(details?.stats?[5].baseStat ?? 0), statValueText: details?.stats?[5].baseStat ?? 0)
                            
                            BarView(color: "\(typesArray.last ?? "normal")", stat: "Attack", statValue: CGFloat(details?.stats?[4].baseStat ?? 0), statValueText: details?.stats?[4].baseStat ?? 0)
                            
                            BarView(color: "\(typesArray.last ?? "normal")", stat: "Defense", statValue: CGFloat(details?.stats?[3].baseStat ?? 0), statValueText: details?.stats?[3].baseStat ?? 0)
                            
                            BarView(color: "\(typesArray.last ?? "normal")", stat: "Sp. Atk", statValue: CGFloat(details?.stats?[2].baseStat ?? 0), statValueText: details?.stats?[2].baseStat ?? 0)
                            
                            BarView(color: "\(typesArray.last ?? "normal")", stat: "Sp. Def", statValue: CGFloat(details?.stats?[1].baseStat ?? 0), statValueText: details?.stats?[1].baseStat ?? 0)
                            
                            BarView(color: "\(typesArray.last ?? "normal")", stat: "Speed", statValue: CGFloat(details?.stats?[0].baseStat ?? 0), statValueText: details?.stats?[0].baseStat ?? 0)
                            
                        }
                        
                    }
                }
            }
        }.onAppear() {
            self.networkManager.getDetails(url: "https://pokeapi.co/api/v2/pokemon/\(self.name)") { (details) in
                self.details = details
                self.appendDetailsData()
            }
            self.networkManager.getSpecies(url: "https://pokeapi.co/api/v2/pokemon-species/\(self.name)") { (species) in
                self.species = species
            }
        }
    }
    
    
    
    func appendDetailsData() {
        for type in details?.types ?? [] {
            typesArray.append(type.type?.name ?? "Can't find type")
        }
    }
    
    struct DetailView_Previews: PreviewProvider {
        static var previews: some View {
            DetailView()
        }
    }
}


