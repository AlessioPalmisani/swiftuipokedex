//
//  Pokemon.swift
//  SwiftUIPokedex
//
//  Created by Alessio Palmisani on 19/01/2021.
//

import Foundation

struct Pokemon: Codable, Identifiable {
    let id = UUID()
    let count: Int?
    let previous, next: String?
    let results: [Result]?
    let details: [Details]?
}

struct Result: Codable, Identifiable {
    let id = UUID()
    let name: String?
    let url: String?
}


