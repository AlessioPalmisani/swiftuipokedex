//
//  Detail.swift
//  SwiftUIPokedex
//
//  Created by Alessio Palmisani on 19/01/2021.
//

import Foundation

struct Details: Codable, Identifiable {
    
    let id: Int?
    let name: String?
    let stats: [Stat]?
    let sprites: Sprites?
    let types: [TypeElement]?
    let results: [Result]?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case types
        case stats
        case sprites
        case results
    }

}

struct Species: Codable, Identifiable {
    let id: Int?
    let name: String?
}

struct TypeElement: Codable {
    let slot: Int?
    let type: Species?
}



struct Stat: Codable{
    let baseStat, effort: Int?

    enum CodingKeys: String, CodingKey {
        case baseStat = "base_stat"
        case effort
    }
}

struct Sprites: Codable {
    let backFemale, backShinyFemale, backDefault, frontFemale: String?
    let frontShinyFemale, backShiny, frontDefault, frontShiny: String?
    let other: Other?

    enum CodingKeys: String, CodingKey {
        case backFemale = "back_female"
        case backShinyFemale = "back_shiny_female"
        case backDefault = "back_default"
        case frontFemale = "front_female"
        case frontShinyFemale = "front_shiny_female"
        case backShiny = "back_shiny"
        case frontDefault = "front_default"
        case frontShiny = "front_shiny"
        case other = "other"
    }
}

struct Other: Codable {
    let officialArtwork: OfficialArtwork?
    
    enum CodingKeys: String, CodingKey {
        case officialArtwork = "official-artwork"
    }
}


struct OfficialArtwork: Codable {
    let front_default: String?
}
