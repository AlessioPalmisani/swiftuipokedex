//
//  Type.swift
//  SwiftUIPokedex
//
//  Created by Alessio Palmisani on 19/01/2021.
//

import Foundation


struct Types: Codable {
    let id: Int?
    let name: String?
}
