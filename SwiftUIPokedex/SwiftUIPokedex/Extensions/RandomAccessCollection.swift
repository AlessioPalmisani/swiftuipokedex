//
//  RandomAccessCollection.swift
//  SwiftUIPokedex
//
//  Created by Alessio Palmisani on 21/01/2021.
//

import Foundation
import SwiftUI

extension RandomAccessCollection where Self.Element: Identifiable {
    func isLastItem<Detail: Identifiable>(_ item: Detail) -> Bool {
        guard !isEmpty else {
            return false
        }
        
        guard let itemIndex = firstIndex(where: { $0.id.hashValue == item.id.hashValue }) else {
            return false
        }
        
        let distance = self.distance(from: itemIndex, to: endIndex)
        return distance == 1
    }
}
