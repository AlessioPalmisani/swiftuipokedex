//
//  PokeListView.swift
//  SwiftUIPokedex
//
//  Created by Alessio Palmisani on 21/01/2021.
//

import SwiftUI

struct PokeListView: View {
    
    
    @ObservedObject var homeDataSource: HomeDataSource
    @State private var isLoading: Bool = false
    @State private var page: Int = 1
    private let pageSize: Int = 25
    
    var body: some View {
        NavigationView {
            List(homeDataSource.details){
                detail in
                NavigationLink(destination: DetailView(name: detail.name ?? "")) {
                    HStack {
                        UrlImageView(urlString: detail.sprites?.frontDefault)
                        Text("#\(String(detail.id ?? 0))")
                        Text(detail.name?.capitalized ?? "")
                        
                        if isLoading && homeDataSource.details.isLastItem(detail) {
                            Divider()
                            ProgressView()
                        }
                    }
                    .onAppear() {
                        listItemAppears(detail)
                    }
                }
            }.navigationBarTitle("Pokedex")
        }.accentColor(.black)
        //        NavigationView{
        //           return List(detailsArray){ detail in
        //                NavigationLink(destination: DetailView(name: detail.name ?? "")){
        //                    HStack {
        //                        UrlImageView(urlString: detail.sprites?.frontDefault)
        //                        Text("#1")
        //                        Text(detail.name ?? "")
        //                    }
        //                }.navigationBarTitle("Pokedex")
        //            }.accentColor(.white)
        //
        //        NavigationView{
        //             List(detailsArray.indices(), self: \.id) { index in
        //                NavigationLink(destination: DetailView(name: detailsArray[index].name)) {
        //                    HStack {
        //                        Text("#\(homeDataSource.pkmnId[index])")
        //                        Text(homeDataSource.names[index].capitalized)
        //                    }
        //                }
        //            }.navigationBarTitle("Pokedex")
        //        }.accentColor(.white)
        
    }
}

private extension PokeListView {
    
 func listItemAppears<Detail: Identifiable>(_ detail: Detail) {
    if !isLoading && self.homeDataSource.details.isLastItem(detail){
            isLoading = true
            page += 1
            getMoreItems(forPage: page, pageSize: pageSize)
            isLoading = false
        }
    }
    
}


private extension PokeListView {
    
    
    func getMoreItems(forPage page: Int, pageSize: Int) {

        let max = ((page * pageSize)) - 1
        homeDataSource.getPokemonWithPage2(min: homeDataSource.details.count , max: max)
    }
}



//struct PokeListView_Previews: PreviewProvider {
//    static var previews: some View {
//        PokeListView(, homeDataSource: <#Binding<HomeDataSource>#>)
//    }
//}
