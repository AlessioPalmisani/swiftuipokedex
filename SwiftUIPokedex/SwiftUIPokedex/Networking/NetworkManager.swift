//
//  NetworkManager.swift
//  SwiftUIPokedex
//
//  Created by Alessio Palmisani on 19/01/2021.
//

import Foundation

enum NetworkResponse:String {
    case success
    case authenticationError = "Authentication Error"
    case badRequest = "Bad Request"
    case outdated = "URL Outdated"
    case failed = "Network Request Failed"
    case noData = "Response data -> Void"
    case unableToDecode = "Data cannot be decoded"
}

enum HTTPResult<String>{
    case success
    case failure(String)
}

class NetworkManager: ObservableObject {
    
    @Published var results = [Result]()
    @Published var names: [String] = []
    @Published var urls: [String] = []
    @Published var details = [Details]()
    
    func fetchPokemonData(completion: @escaping ([Result]) -> ()) -> () {
        guard let url = URL(string: "https://pokeapi.co/api/v2/pokemon/?limit=2000") else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                print("Connection Unavailable, please check network settings")
            }
            
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                
                switch result {
                case .success:
                    guard let responseData = data else {
                        print(NetworkResponse.noData.rawValue)
                        return
                    }
                    do {
                        print(responseData)
                        
                        let apiResponse = try JSONDecoder().decode(Pokemon.self, from: responseData)
                        DispatchQueue.main.async {
                            completion(apiResponse.results ?? [])
                        }
                    } catch {
                        print(error)
                        print(NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    print(networkFailureError)
                    
                }
                
            }
        }
        .resume()
    }
    
    func fetchPokemonDataPaged(limit: Int = 25, offset: Int = 0 ,completion: @escaping ([Result]) -> ()) -> () {
        guard let url = URL(string: "https://pokeapi.co/api/v2/pokemon/?Offset=\(offset)&limit=\(limit)") else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                print("Connection Unavailable, please check network settings")
            }
            
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                
                switch result {
                case .success:
                    guard let responseData = data else {
                        print(NetworkResponse.noData.rawValue)
                        return
                    }
                    do {
                        print(responseData)
                        
                        let apiResponse = try JSONDecoder().decode(Pokemon.self, from: responseData)
                        DispatchQueue.main.async {
                            completion(apiResponse.results ?? [])
                        }
                    } catch {
                        print(error)
                        print(NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    print(networkFailureError)
                    
                }
                
            }
        }
        .resume()
    }
    
    func getDetailsArray(url: String, completion: @escaping ([Details]) -> ()) {
        
        guard let url = URL(string: url) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print("Please check your network connection.")
            }
            
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        print(NetworkResponse.noData.rawValue)
                        return
                    }
                    do {
                        print(responseData)
                        
                        let apiResponse = try JSONDecoder().decode(Pokemon.self, from: responseData)
                        DispatchQueue.main.async {
                            completion(apiResponse.details ?? [])
                        }
                    } catch {
                        print(error)
                        print(NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    print(networkFailureError)
                }
            }
        }
        .resume()
    }
    
    func getDetails(url: String, completion: @escaping (Details) -> ()) {
        
        guard let url = URL(string: url) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print("Please check your network connection.")
            }
            
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        print(NetworkResponse.noData.rawValue)
                        return
                    }
                    do {
                        print(responseData)
                        
                        let apiResponse = try JSONDecoder().decode(Details.self, from: responseData)
                        DispatchQueue.main.async {
                            completion(apiResponse)
                        }
                    } catch {
                        print(error)
                        print(NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    print(networkFailureError)
                }
            }
        }
        .resume()
    }
    
   
    
    func getSpecies(url: String, completion: @escaping (Species) -> ()) {
        
        guard let url = URL(string: url) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print("Please check your network connection.")
            }
            
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        print(NetworkResponse.noData.rawValue)
                        return
                    }
                    do {
                        print(responseData)
                        
                        let apiResponse = try JSONDecoder().decode(Species.self, from: responseData)
                        DispatchQueue.main.async {
                            completion(apiResponse)
                        }
                    } catch {
                        print(error)
                        print(NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    print(networkFailureError)
                }
            }
        }
        .resume()
    }
    
    func getTypes(url: String, completion: @escaping (Types) -> ()) {
        
        guard let url = URL(string: url) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print("Please check your network connection.")
            }
            
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        print(NetworkResponse.noData.rawValue)
                        return
                    }
                    do {
                        print(responseData)
                        
                        let apiResponse = try JSONDecoder().decode(Types.self, from: responseData)
                        DispatchQueue.main.async {
                            completion(apiResponse)
                        }
                    } catch {
                        print(error)
                        print(NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    print(networkFailureError)
                }
            }
        }
        .resume()
    }
    
    
    
    func appendResultsData() {
        for result in results {
            names.append(result.name ?? "")
            urls.append(result.url ?? "")
        }
    }
    
    
    
    func handleNetworkResponse(_ response: HTTPURLResponse) -> HTTPResult<String> {
        print(response.statusCode)
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
        case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
        case 600: return .failure(NetworkResponse.outdated.rawValue)
        default: return .failure(NetworkResponse.failed.rawValue)
        }
        
    }
}
