//
//  HomeDataSource.swift
//  SwiftUIPokedex
//
//  Created by Alessio Palmisani on 19/01/2021.
//

import SwiftUI

class HomeDataSource: ObservableObject {
    
    @Published var names: [String] = []
    @Published var results: [Result] = []
    @Published var id: [String] = []
    @Published var pkmnId: [String] = []
    @Published var details: [Details] = []
    
    init() {
         getPokemon()
//         print(names)
//        for (_, name) in names.enumerated() {
//            getDetails(index: index(<#T##UnsafePointer<Int8>!#>, <#T##Int32#>))
//        }
    }
    
    func getPokemon() {
        NetworkManager().fetchPokemonDataPaged { (results) in
            self.results = results
            self.appendResultsData()
            for (index, _) in self.names.enumerated() {
                self.getDetails(index: index)
                
            }
            
        }
        
    }
    
    func getPokemonWithPage(min: Int, max: Int) -> [Details]{
        var detailArr: [Details] = []
        
        for i in min...max {
            NetworkManager().getDetails(url: "https://pokeapi.co/api/v2/pokemon/\(String(i))") { detail in
                detailArr.append(detail)
                detailArr.sort{$0.id ?? 0 < $1.id ?? 0}
            }
        }
        return detailArr
    }
    
    func getPokemonWithPage2(min: Int, max: Int){
print ("Values: \(min), \(max)")
        for i in min...max {
            NetworkManager().getDetails(url: "https://pokeapi.co/api/v2/pokemon/\(String(i))") { detail in
                self.details.append(detail)
                self.details.sort{$0.id ?? 0 < $1.id ?? 0}
            }
        }
     
    }
    
    func getDetails(index: Int) {
        NetworkManager().getDetails(url: "https://pokeapi.co/api/v2/pokemon/\(String(index+1))") { detail in
            self.details.append(detail)
            self.details.sort{$0.id ?? 0 < $1.id ?? 0}
        }
    }

    
    
    func appendResultsData() {
        
        var url: [String] = []
        
        for result in results {

            names.append(result.name ?? "")
            let pkmnIdString = String(result.url?.dropFirst(34) ?? "").dropLast()
            if Int(pkmnIdString) ?? 0 > 898 {
                let actual = 898
                let new = (Int(pkmnIdString.dropFirst()) ?? 0) + actual
                pkmnId.append(String(new))
            } else {
                pkmnId.append(String(pkmnIdString))
            }
            url.append(String(result.url?.dropFirst(34) ?? ""))
            
            
        }
        
        for ids in url {
            id.append(String(ids.dropLast()))
        }
    }
    
}
